# You dare to use my own best practices against me, Intern ?


## RestFulll best practices

First we gonna ask ourselves how to get the type(verb) of the request in PHP

To know the type of the request check the code available under `/test/request_type.php`

### Using GET to GET the data
1. In `/users/get.php` edit the code so that the routes will only return data if the method was a GET method and return nothing otherwise
2. In `/users/index.php` edit the code so that the routes will only return data if the method was a GET method and return nothing otherwise

> Hint when using die() the program will end

### Using POST to create the data
1. In `/users/create.php` edit the code so that the routes will only return data if the method was a POST method and return nothing otherwise
2. Switch the way of getting the variables values (`name`, `email`, `password` ) from using $_GET to using $_POST
3. If you wants to test if this routes is working you need to test it using postman

### Using PUT to update the data
1. In `/users/update.php` edit the code so that the routes will only return data if the method was a PUT method and return nothing otherwise
2. Switch the way of getting the variables values (`name`, `email`, `password` ) from using $_GET to using $_POST
3. Keep on getting the id variable from the `$_GET['id']` just because in restfull API the id should be present in the URL
4. If you wants to test if this routes is working you need to test it using postman

### Using PUT to update the data
1. In `/users/update.php` edit the code so that the routes will only return data if the method was a PUT method and return nothing otherwise
2. Keep on getting the id variable from the `$_GET['id']` just because in restfull API the id should be present in the URL
3. If you wants to test if this routes is working you need to test it using postman

## namespaces: what are namespaces

Before starting take sometimes to read the following namespace introduction
[https://www.the-art-of-web.com/php/implementing-namespaces/](https://www.the-art-of-web.com/php/implementing-namespaces/)


### Change code

1. Inside `/model/db.php` lets declare a namesapace called: `CodiFinancialApp\Model\DB`
2. Inside `/model/User.php` lets declare a namespace called `CodiFinancialApp\Model\User`
3. Inside each files under the `users` directory change the code to use the namespace declared above
4. You will still need to use the require of the file 
5. the `use **namespace**` should be as the first start of the file, directly under the opening tag of PHP

### Practice 

Replicate the User model and users routes for the Currencies table

Reminder on the currencies table structure: id, country, name, code, symbol

Criteria to check on create:
        
        1. coutry is required and should not be empty
        2. country is a unique field so on update you need to have a validation that check if the country does exist on other rows?
        3. Code is required and can't be empty
        3. Symbol is required and can't be empty
        
Criteria to check on update:

        1. coutry is optional and if available it should not be empty
        2. country is a unique field so on update you need to have a validation that check if the country does exist on other rows?
        3. Code is optional and if available it can't be empty
        3. Symbol is optional and if available it can't be empty
        
### Test

1. GET: http://localhost:8080/currencies `=>` should get all the currencies
2. GET: http://localhost:8080/currencies/get.php?id=2 `=>` should get one currencies that have the id 2
3. POST: http://localhost:8080/currencies/create.php `=>` should create a new currency if the criteria of the parameters were met (remember to check the `users/create.php`) if validation failed you should return an error
4. PUT: http://localhost:8080/currencies/update.php `=>` should return an error because the id is not available in the url (check the users/update.php)
5. PUT: http://localhost:8080/currencies/update.php?id= `=>` should return an error because the id is available in the url but empty (check the users/update.php)
6. PUT: http://localhost:8080/currencies/update.php?id=2 `=>` should check if the id was available in the currencies tables if it wasn't return an erorr, if it was available we continue into validating the field using the update criteria above
7. DELETE: http://localhost:8080/currencies/delete.php `=>` should return an error because the id is not available in the url (check the users/delete.php)
8. DELETE: http://localhost:8080/currencies/delete.php?id= `=>` should return an error because the id is available in the url but empty (check the users/delete.php)
9. DELETE: http://localhost:8080/currencies/delete.php?id=2 `=>`should check if the id was available in the currencies database, if it wasn't available it should return an error, if it was available it should delete the currency with provided id


## Authentication

1. Before proceeding do a `git add -A`, `git commit -m "Back: DB mode done, namespace ready, currecnies & users api"`
2. Before proceeding do a `git push`, `git checkout master`, `git pull`, `git checkout your-branch`, `git merge master`