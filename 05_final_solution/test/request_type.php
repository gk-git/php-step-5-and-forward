<?php

$method = $_SERVER['REQUEST_METHOD'];

switch ($method) {
    case 'GET':
        echo 'This is a GET request';
        break;
    case  'POST':
        echo 'This is a POST request';
        break;
    case  'PUT':
        echo 'This is a PUT request';
        break;
    case  'PATCH':
        echo 'This is a PATCH request';
        break;
    case  'DELETE':
        echo 'This is a DELETE request';
        break;
    default:
        echo 'Default: This is a ' . $method .' request';

}