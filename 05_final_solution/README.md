# Final Solution

## IMPORTANT Clone this repo

## Create your own branch

## work on your branch

## If you don't do that you will face problems at the authentication part

## What we have till now?

---
1. A helpers directory that contains functions that doesn't interact with the logic
    1. `cors.php` contain the code that by pass the `cors` security
    1. `helpers.php` contain functions that aren't related to the logic of the application and that doesn't need to be in a separate directory
2. A model directory that contains all the files related to communicate with the database
    1. `db.php` contains the code that connect our app to the database
    2. `User.php` contains the code that deal with communicating with ours `users` table


## Naming convention

---
### DB Model
1. The file name should be singular and it should follows the pascale case (first letter of each word should be capital)
2. The table name should be plural and lowercase, if the table name is a combination of multiple words (2 or more), we separate each word by an underscore `_`
3. When creating an instance of a DB model the variable name should be a combination of the model file name plus the word `Model`
    1. Example: `User.php` => `$UserModel = new User($db);` |||  `Currency.php` => `$CurrencyModel = new Currency($db);`

## Request Roadmap

---
### Enable Cross-origin resource sharing (Cors)

By requiring the cors file using `require_once __DIR__ . '/../helpers/cors.php';`
we are adding some security to our application 


## Improvement and whats is missing

---

### Improvement

On following the ReetFull api architecture we are missing the below 2 rules, for now we will just use the right verbs 
and for the second rule we will apply it when we go into laravel

1. Using the right verbs for each API request a verb (GET: To get data, POST: to add data, PUT/PATCH: to update data, DELETE: to delete data)
2. Change url structure to not use (create, delete, get. update in the url)
    1. GET: /users/create.php?name=test&email=test@t.com&passsword=1234
        1.POST: /users/
    2. GET: /users/delete.php?id=1
        1. DELETE: /users/1
    3. GET: /users/get.php?id=2
        GET: /users/2
    4. GET:: /users
        1. GET: /users
    5. GET: /users/update.php?id=2&name=gaby
        1. PUT: /users/2
3. What would happen if we declare the same class name in 2 seperate file and we import thus 2 files in the same code.
    
    Example:
    >File 1: /model/User.php
    ```php
   class User {
   // do something
    }
    ```
   
   > File 2: /controllers/User.php
   ```php
    class User {
   // do something
    }
    ``` 
   > File 3: /index.php
   ```php
     require_once '/model/User.php';
     require_once '/controllers/User.php';
   
   // Result name conflict => :fireworks: :fireworks: :fireworks: :fireworks:
   ```
   :fireworks: :fireworks: :fireworks:
   
   Solution ? use something called namespace? what you need to know about namespace is that its a solution to prevent naming conflict
   
### Missing

On validation process we are missing some validation on the following API requests:

1. GET user by id:
    1. We don't have any validation to check if the id was passed in the URL
2. DELETE user by id:
    1. We don't have any validation to check if the id was passed in the URL
3. UPDATE user by id:
    1. We don't have any validation to check if the id was passed in the URL
    2. We haven't added any validation to check if user name is not an empty string    
    3. We haven't added any validation to check if user email is a valid email
4. Create user:
    1. We haven't told the users which field is missing instead we just told him that all the fields are required


On Authentication we have created a route for login but we haven't implement it yet 
and we didn't create a function that check if the user is authenticated

>>>>> Step by Step solution [click me](./solution.md)


